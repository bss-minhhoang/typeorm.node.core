import { Request, Response, NextFunction } from 'express';
import * as passport  from 'passport';
import { getCustomRepository } from 'typeorm';
import * as request from 'request';
import { validate } from 'class-validator'

import UserRepository from '../repositories/user';
import { responseHelper, jwtHelper } from '../utils';
import { facebookConfig, twitterConfig } from '../config/passport/config';

export default class AuthController {

    /*///////////////////////////////////////////////////////////////
    /////                    START LOCAL LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/

    // Local login
    static async signIn(req: Request, res: Response, next: NextFunction): Promise<any>{
        const userRepository = getCustomRepository(UserRepository);
        // Local authentication
        passport.authenticate('local', {
            session: false
        }, async (err, rs) => {
            // Error, pass to the next middleware
            if (err) {
                return responseHelper.returnUnAuthorizeResponse(res, err.errors);
            }

            // Generate jwt token
            rs.data.token = jwtHelper.signToken(rs.data);

            // Save token to user
            await userRepository.save(rs.data);

            return responseHelper.returnSuccessResponse(res, rs.data);
        })(req, res, next);
    };

    // Sign up
    static async signUp(req: Request, res: Response, next: NextFunction): Promise<any>{
        const userRepository = getCustomRepository(UserRepository);
        const userInstance =  userRepository.create();

       // Check if whether user existed already
       const existingUser = await userRepository.findOne({username: req.body.username});

        // Return error
        if(existingUser){
            return responseHelper.returnDuplicateResponse(res, 'email.existed')
        }

        // Create new user base on data in body
        userInstance.email = req.body.email;
        userInstance.password = req.body.password;

        const rs = await userRepository.save(userInstance);

        // Remove password property
        delete rs.password;

        return responseHelper.returnSuccessResponse(res, rs)

    };


    /*///////////////////////////////////////////////////////////////
    /////                    END LOCAL LOGIN                   /////
    ///////////////////////////////////////////////////////////////*/




    /*///////////////////////////////////////////////////////////////
    /////                  START FACEBOOK LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/

    // Login by facebook
    static async facebookLogin(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('facebook', {
            session: false
        })(req, res, next);
    };

    // Facebook login callback
    static async facebookLoginCallback(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('facebook', {
            session: false
        }, async(err, facebookData) => {
            try {
                // Error, pass to the next middleware
                if (err) {
                    return next(err);
                }
                const { profile } = facebookData;

                let rs = await AuthController.createUser(req, res, profile);

                return responseHelper.returnSuccessResponse(res, rs)
            } catch (err) {
                return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
            }
        })(req, res, next);
    }

    // Validate and get user data from facebook token
    static async validateFromFacebookToken(req: Request, res: Response, next: NextFunction): Promise<any>{

        // Basic configs
        const PROVIDER = 'facebook'; // Provider
        const fields = [ 'id', 'email', 'birthday', 'gender', 'picture' ]; // Field scope
        const graphApiUrl = `https://graph.facebook.com/v3.1/me?fields=${ fields.join(',')}`; // Prepare APIs
        // Prepare params
        const params = {
            access_token: req.body.accessToken,
            client_id: facebookConfig.clientID,
            client_secret: facebookConfig.clientSecret,
            redirect_uri: facebookConfig.callbackURL
        };

        try {
            if (!req.body.accessToken) {
                return responseHelper.returnInternalServerResponse(res, 'facebook.token.required');
            }

            // Request user profile from facebook APIs by access tokens
            request.get({
                url: graphApiUrl,
                qs: params,
                json: true
            }, async(err, response, profile) => {
                // Validate error from facebook
                if (response.statusCode !== 200) {
                    return responseHelper.returnInternalServerResponseWithMessage(res, 'facebook.validate.error', profile.error.message)
                }

                // Update provider
                profile.provider = PROVIDER;

                // Create the new user
                let rs = await AuthController.createUser(req, res, profile);

                return responseHelper.returnSuccessResponse(res, rs)
            });
        } catch (err) {
            return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
        }
    }

    /*///////////////////////////////////////////////////////////////
    /////                   END FACEBOOK LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/



    /*///////////////////////////////////////////////////////////////
    /////                  START TWITTER LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/

    // Login by twitter
    static async twitterLogin(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('twitter', {
            session: false
        })(req, res, next);
    };

    // Twitter login callback
    static async twitterLoginCallback(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('twitter', {
            session: true
        }, async(err, twitterData) => {
            try {
                // Error, pass to the next middleware
                if (err) {
                    return next(err);
                }
                const { profile } = twitterData;

                let rs = await AuthController.createUser(req, res, profile);

                return responseHelper.returnSuccessResponse(res, rs)
            } catch (err) {
                return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
            }
        })(req, res, next);
    }

    // Validate and get user data from twitter token
    static async validateFromTwitterToken(req: Request, res: Response, next: NextFunction): Promise<any>{

        // Basic configs
        const PROVIDER = 'twitter'; // Provider
        const profileApiUrl = 'https://api.twitter.com/1.1/account/verify_credentials.json';
        // Prepare params
        const params = {
            consumer_key: twitterConfig.consumerKey,
            consumer_secret: twitterConfig.consumerSecret,
            token: req.body.access_token,
            token_secret: req.body.refreshToken
        };

        try {
            if (!req.body.accessToken || !req.body.refreshToken) {
                return responseHelper.returnInternalServerResponse(res, 'twitter.token.required');
            }

            // Request user profile from facebook APIs by access tokens
            request.get({
                url: profileApiUrl,
                qs: {
                    include_email: true
                },
                oauth: params
            }, async(err, response, profile) => {
                // Validate error from facebook
                if (response.statusCode !== 200 || err) {
                    return responseHelper.returnInternalServerResponseWithMessage(res, 'twitter.validate.error', profile.error.message)
                }

                // Update provider
                profile.provider = PROVIDER;

                // Create the new user
                let rs = await AuthController.createUser(req, res, profile);

                return responseHelper.returnSuccessResponse(res, rs)
            });
        } catch (err) {
            return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
        }
    }

    /*///////////////////////////////////////////////////////////////
    /////                   END TWITTER LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/



    /*///////////////////////////////////////////////////////////////
    /////                 START INSTAGRAM LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/

    // Login by instagram
    static async instagramLogin(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('instagram', {
            session: false
        })(req, res, next);
    };

    // Instagram login callback
    static async instagramLoginCallback(req: Request, res: Response, next: NextFunction): Promise<any>{
        passport.authenticate('instagram', {
            session: false
        }, async(err, instagramData) => {
            try {
                // Error, pass to the next middleware
                if (err) {
                    return next(err);
                }
                const { profile } = instagramData;

                let rs = await AuthController.createUser(req, res, profile);

                return responseHelper.returnSuccessResponse(res, rs)
            } catch (err) {
                return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
            }
        })(req, res, next);
    }

    // Validate and get user data from instagram token
    static async validateFromInstagramToken(req: Request, res: Response, next: NextFunction): Promise<any>{

        // Basic configs
        const PROVIDER = 'instagram'; // Provider
        const graphApiUrl = `https://api.instagram.com/v1/users/self/?access_token=${ req.body.accessToken}`; // Prepare APIs

        try {
            if (!req.body.accessToken) {
                return responseHelper.returnInternalServerResponse(res, 'facebook.token.required');
            }

            // Request user profile from facebook APIs by access tokens
            request.get({
                url: graphApiUrl
            }, async(err, response, profile) => {
                // Validate error from facebook
                if (response.statusCode !== 200) {
                    return responseHelper.returnInternalServerResponseWithMessage(res, 'facebook.validate.error', profile.error.message)
                }

                // Update provider
                profile.data.provider = PROVIDER;

                // Create the new user
                let rs = await AuthController.createUser(req, res, profile.data);

                return responseHelper.returnSuccessResponse(res, rs)
            });
        } catch (err) {
            return responseHelper.returnInternalServerResponseWithMessage(res, 'system.internal.error', err.message);
        }
    }

    /*///////////////////////////////////////////////////////////////
    /////                   END INSTAGRAM LOGIN                 /////
    ///////////////////////////////////////////////////////////////*/


    // Create user
    static async createUser(req: Request, res: Response, profile: any): Promise<any> {
        const userRepository = getCustomRepository(UserRepository);
        const userId = profile.id || profile.sub;

        // Check if whether user existed in system already
        let userData = await userRepository.findOne({socialId: userId});

        // User existed already, create the new token and throw back to user
        if (userData) {
            // Generate jwt token
            userData['token'] = jwtHelper.signToken(profile);

            // Update token to user data
            return await userRepository.save(userData);
        }

        let insertData = {};

        // Normalize data with specific strategy
        switch (profile.provider) {
            case 'facebook': {
                insertData = {
                    fullName: profile.displayName,
                    socialId: profile.id,
                    email: profile.email ? profile.email : profile.emails[0].value,
                    provider: profile.provider,
                    birthday: profile.birthday ? profile.birthday : profile._json.birthday,
                    gender: profile.gender ? profile.gender : profile._json.gender,
                    avatarUrl: profile.picture ? profile.picture.data.url : profile._json.picture.data.url,
                    username: profile.email ? profile.email : profile.emails[0].value
                };

                break;
            }
            case 'twitter': {
                insertData = {
                    fullName: profile.displayName,
                    socialId: profile.id,
                    provider: profile.provider,
                    avatarUrl: profile._json.profile_image_url,
                    username: profile.username
                };
                break;
            }

            case 'instagram': {
                insertData = {
                    fullName: profile.displayName ? profile.displayName : profile.full_name,
                    socialId: profile.id,
                    provider: profile.provider,
                    avatarUrl: profile._json ? profile._json.data.profile_picture : profile.profile_picture,
                    username: profile._json ? profile._json.data.username : profile.username
                };

                break;
            }


            case 'google': {
                break;
            }
        }

        // Generate jwt token
        insertData['token'] = jwtHelper.signToken(profile);

        // Create the new user and throw token back
        return await userRepository.save(insertData);
    };
}

