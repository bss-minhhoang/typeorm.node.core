import { Request, Response, NextFunction } from "express";
import { getCustomRepository } from "typeorm";
import UserRepository from "../repositories/user";
import { responseHelper } from "../utils";

export default class UserController {

    // Create user with name
    static async createUserWithName(req: Request, res: Response, next: NextFunction){
        const userRepository = getCustomRepository(UserRepository);
        const user = userRepository.create(); // same as const user = new User();
        const bodyRequest = req.body;


        user.firstName = bodyRequest.firstName;
        user.lastName = bodyRequest.lastName;

        try{
            // Save to db
            const userData = await userRepository.save(user);

            return responseHelper.returnSuccessResponse(res, userData)

        }catch (e) {
            return responseHelper.returnInternalServerResponse(res, new Error(`Error: ${e}`))
        }


    };

    // Get user detail by Id
    static async getUserById(req: Request, res: Response, next: NextFunction){
        const userRepository = getCustomRepository(UserRepository);
        const requestParams = req.params;

        try{
            const userData = await userRepository.findOne(requestParams.id);

            return responseHelper.returnSuccessResponse(res, userData)

        }catch (e) {
            return responseHelper.returnInternalServerResponse(res, new Error(`Error: ${e}`))
        }

    };

    // Get user list
    static async getUserList(req: Request, res: Response, next: NextFunction){
        const userRepository = getCustomRepository(UserRepository);

        try{
            const userData = await userRepository.find();

            return responseHelper.returnSuccessResponse(res, userData)

        }catch (e) {
            return responseHelper.returnInternalServerResponse(res, new Error(`Error: ${e}`))
        }

    };
}

