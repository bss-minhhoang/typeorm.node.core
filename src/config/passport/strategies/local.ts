import * as passportLocal from 'passport-local';
import * as passport from 'passport';
import { validate } from 'class-validator'

import { getCustomRepository } from 'typeorm';
import UserRepository from '../../../repositories/user';
import { normalizeError } from '../../../utils'

const LocalStrategy = passportLocal.Strategy;

export default () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      async (email, password, done) => {
        try {
            const userRepository = getCustomRepository(UserRepository);
            const userInstance =  userRepository.create();

            userInstance.email = email;
            userInstance.password = password;

            // Validate input data
            const validateResults = await validate(userInstance, {
                groups: ['login'],
                validationError: { target: false }
            });

            const errorList = normalizeError(validateResults);

            // Error
            if(errorList.length > 0){
                return done({
                    errors: errorList
                },null);
            }


            // Find user by email
            const user = await userRepository.findOne({ email: email });

            // Check email existed or not
            if (!user) {
                return done({
                    errors: ['email.not.existed']
                },null);
            }


            // Check if matched password
            const isMatch = await userRepository.validatePassword(password, user.password);

            // Not matched password, callback error
            if (!isMatch) {
                return done({
                    errors: ['password.not.matched']
                }, null);
            }

            // Remove password property
            delete user.password;

            return done(null, {
                data: user
            });
        } catch (err) {
          return done(err, null);
        }
      }
    )
  );
};
