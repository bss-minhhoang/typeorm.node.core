import * as express from 'express';

import authRoute from './auth'
import userRoute from './user';

const v1 = express.Router();

// Default public route
const publicRoute = (app)=>{
    // Auth APIs
    v1.use('/auth',authRoute);

    // User APIs
     v1.use('/user',userRoute);

    app.use("/api/v1", v1);
};

export default publicRoute;