import * as express from 'express';

import UserController from '../controllers/user'

const userRoute = express.Router();

// Get user list
userRoute.route('/')
    .get(UserController.getUserList);

// Get user detail by Id
userRoute.route('/:id')
    .get(UserController.getUserById);

// Create new user
userRoute.route('/')
    .post(UserController.createUserWithName);

export default userRoute;