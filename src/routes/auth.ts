import * as express from 'express';

import AuthController from '../controllers/auth'

const authRoute = express.Router();

/*///////////////////////////////////////////////////////////////
/////                  START FACEBOOK LOGIN                 /////
///////////////////////////////////////////////////////////////*/

// Facebook Login
authRoute.route('/facebook')
    .get(AuthController.facebookLogin);

// Facebook login callback
authRoute.route('/facebook/callback')
    .get(AuthController.facebookLoginCallback);

// Verify facebook from access token
authRoute.route('/facebook/validate')
    .post(AuthController.validateFromFacebookToken);


/*///////////////////////////////////////////////////////////////
/////                  STOP FACEBOOK LOGIN                 /////
///////////////////////////////////////////////////////////////*/



/*///////////////////////////////////////////////////////////////
/////                  START TWITTER LOGIN                 /////
///////////////////////////////////////////////////////////////*/

// Twitter Login
authRoute.route('/twitter')
    .get(AuthController.twitterLogin);

// Twitter login callback
authRoute.route('/twitter/callback')
    .get(AuthController.twitterLoginCallback);

// Verify twitter from access token
authRoute.route('/twitter/validate')
    .post(AuthController.validateFromTwitterToken);


/*///////////////////////////////////////////////////////////////
/////                   END TWITTER LOGIN                   /////
///////////////////////////////////////////////////////////////*/



/*///////////////////////////////////////////////////////////////
/////                 START INSTAGRAM LOGIN                 /////
///////////////////////////////////////////////////////////////*/

// Twitter Login
authRoute.route('/instagram')
    .get(AuthController.instagramLogin);

// Twitter login callback
authRoute.route('/instagram/callback')
    .get(AuthController.instagramLoginCallback);

// Verify twitter from access token
authRoute.route('/instagram/validate')
    .post(AuthController.validateFromInstagramToken);


/*///////////////////////////////////////////////////////////////
/////                 END INSTAGRAM LOGIN                   /////
///////////////////////////////////////////////////////////////*/




/*///////////////////////////////////////////////////////////////
/////                    START LOCAL LOGIN                 /////
///////////////////////////////////////////////////////////////*/

// Local login
authRoute.route('/signIn')
    .post(AuthController.signIn);

// Local sign up
authRoute.route('/signUp')
    .post(AuthController.signUp);


/*///////////////////////////////////////////////////////////////
/////                     END LOCAL LOGIN                 /////
///////////////////////////////////////////////////////////////*/

export default authRoute;