import { errorCodes } from '../../constants'

/* HTTP RESPONSE ERROR CODE */
const SUCCESS_REQUEST = 200;

const BAD_REQUEST_ERROR = 400;
const UNAUTHORIZED_ERROR = 401;
const ENTITY_DUPLICATION_ERROR = 403;
const NOT_FOUND_ERROR = 404;
const INTERNAL_SERVER_ERROR = 500;

const responseHelper = {
    // Success returned
    returnSuccessResponse: (res: any, data: any) => {
        return res.status(SUCCESS_REQUEST).json({
            error: false,
            data,
            errors: []
        });
    },

    // Not found error returned
    returnNotFoundResponse: (res: any, errors: any) => {
        errors = responseHelper.getErrorByCode(errors);
        return res.status(NOT_FOUND_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    // Bad request error returned
    returnBadRequestResponse: (res: any, errors: any) => {
        errors = responseHelper.getErrorByCode(errors);
        return res.status(BAD_REQUEST_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    // Unauthorized error returned
    returnUnAuthorizeResponse: (res: any, errors: any) => {
        errors = responseHelper.getErrorByCode(errors);
        return res.status(UNAUTHORIZED_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    // Duplicated error returned
    returnDuplicateResponse: (res: any, errors: any) => {
        errors = responseHelper.getErrorByCode(errors);
        return res.status(ENTITY_DUPLICATION_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    // Internal server error returned
    returnInternalServerResponse: (res: any, errors: any) => {
        errors = responseHelper.getErrorByCode(errors);
        return res.status(INTERNAL_SERVER_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    returnInternalServerResponseWithMessage: (res: any, errorCode: string, errorMessage: string) => {
        let errors = responseHelper.getErrorByCode(errorCode, errorMessage);
        return res.status(INTERNAL_SERVER_ERROR).json({
            error: true,
            data: null,
            errors
        });
    },

    // Get errors by error code array or string
    getErrorByCode: (codes: any, message? : any) => {
        let errors = [];

        // Code arrays
        if(typeof codes === 'object'){
           codes.map(code => errors.push({
               code: errorCodes[code].code,
               message: errorCodes[code].message
           }))
        }else{ // String
            errors.push({
                code: errorCodes[codes].code,
                message: message ? message: errorCodes[codes].message
            });
        }

        return errors
    }
};

export default responseHelper;