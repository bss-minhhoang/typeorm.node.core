import * as JWT from 'jsonwebtoken';

export default {
    signToken: user => {
        return JWT.sign({
                iss: 'ApiAuth',
                id: user.id,
                iat: new Date().getTime(),
                exp: new Date().setDate(new Date().getDate()) + 3 // 3 days
            },
            process.env.JWT_SECRET
        )
    }
}