import { registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import {getCustomRepository} from "typeorm";
import UserRepository from "../../repositories/user";

export function IsEmailAlreadyExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: IsEmailAlreadyExistConstraint
        });
    };
}

@ValidatorConstraint({ name: "IsEmailAlreadyExist" })
export class IsEmailAlreadyExistConstraint implements ValidatorConstraintInterface {

    validate(value: any, args: ValidationArguments) {
        const userRepository = getCustomRepository(UserRepository);
        return userRepository.findOne({email: value}).then(user => {
            return !user;
        });


    }

}