const errorCodes = {
    'system.unauthorized': {code: 2001, message: 'Unauthorized'},
    'system.internal.error': {code: 2002, message: 'Internal server error'},

    'email.existed': {code: 3001, message: 'The email has been already taken'},
    'email.not.existed': {code: 3002, message: 'Your email or password is not correct'},
    'email.invalid': {code: 3003, message: 'Invalid email format'},
    'password.not.matched': {code: 3004, message: 'Your email or password is not correct'},
    'password.length.invalid': {code: 3005, message: 'Password have to be from 6-20 characters'},

    'facebook.token.required':  {code: 4001, message: 'Access token is required'},
    'facebook.validate.error': {code: 4002, message: 'Error validation from facebook'},
    'twitter.token.required': {code: 4003, message: 'Access or refresh token is required'},
    'twitter.validate.error': {code: 4004, message: 'Error validation from twitter'},
};

export default errorCodes;