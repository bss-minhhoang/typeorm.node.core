import * as bcrypt from 'bcryptjs';
import { EntityRepository, Repository } from "typeorm";
import { User } from "../entities/user";

@EntityRepository(User)
export default class UserRepository extends Repository<User> {

    findByName(firstName: string, lastName: string) {
        return this.findOne({ firstName, lastName });
    }

     async validatePassword(inputPassword: string, userPassword: string, ): Promise<any> {
        try {
            return await bcrypt.compare(inputPassword, userPassword);
        } catch (error) {
            throw new Error(error);
        }
    }

}