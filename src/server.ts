import * as express from 'express';
import * as bodyParser from 'body-parser';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as cors from "cors";
import * as dotenv from "dotenv";

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */

dotenv.config();

// Import passport strategies
import passportStrategies from './config/passport';

// Import routers
import publicRoute from './routes'

/**
 * Create Express server.
 */
const app = express();

/**
 * Express configuration.
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/**
 * Enable CORS
 */
app.use(cors());

/**
 *  Init Passport
 */
passportStrategies(app);

/**
 * Import routers
 */

publicRoute(app);

/**
 * Start Express server.
 */
app.listen(process.env.PORT || 3000, () => {
    console.log(("  App is running at http://localhost:%d in %s mode"), process.env.PORT || 3000, app.get("env"));
    console.log("  Press CTRL-C to stop\n");
});

/**
 * Create database connection, config is read from .env as default
 */
createConnection().then(async connection => {
    console.log("Connected to DB");

}).catch(error => console.log("TypeORM connection error: ", error));