import { BeforeInsert, Column, Entity, ObjectID, ObjectIdColumn } from "typeorm";
import * as bcrypt from 'bcryptjs';
import { Length, IsEmail, IsDate } from 'class-validator';
import  { IsEmailFormat, IsEmailAlreadyExist, IsEmailNotExist }  from '../utils'

@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column({select: false})
    @Length(6, 20, {
        groups: ['login'],
        message: 'password.length.invalid'
    })
    password: string;

    @Column()
    socialId: string;

    @Column()
    @IsEmailFormat({
        groups: ['login'],
        message: 'email.invalid'
    })
    email: string;

    @Column()
    provider: string;

    @Column()
    username: string;

    @Column()
    gender: string;

    @Column()
    @IsDate()
    birthday: string;

    @Column()
    @Length(2, 20)
    firstName: string;

    @Column()
    @Length(2, 20)
    lastName: string;

    @Column()
    @Length(10, 60)
    fullName: string;

    @Column()
    role: string;

    @Column()
    avatarUrl: string;

    @Column()
    token: string;

    @BeforeInsert()
    async hashPassword() {

        // Generate a salt for hash string
        const salt = await bcrypt.genSaltSync(parseInt(process.env.HASH_SALT));


        // Replace raw password by the hashed one
        this.password = await bcrypt.hashSync(this.password, salt);
    }

}
