# Setup

## Prerequisites
   - Install the latest NodeJS from this site
   ```
   https://nodejs.org/en/
   ```
   - Clone this repo and navigate into project folder
## Installing

- Install all related package. At current root project folder, run this command: 
```
npm install
```
- Copy .env from sample .env file
```
 copy env.sample.dev .env
```

- Configure settings in the new .env, point them to local environment if you run this server on local machine.
 
 For example, the following config is used to run server at port 5000, connected to Mongo DB hosted on localhost
```
# Express Config
PORT=5000

# Typeorm Config

TYPEORM_CONNECTION=mongodb
TYPEORM_URL=mongodb://admin:admin123@localhost
TYPEORM_DATABASE=test
TYPEORM_PORT=27017
TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=true
TYPEORM_ENTITIES =entity/.*js,modules/**/entity/.*js


```
## Running app
At the root of project, run this command:
```
  ts-node src/server.ts
```
Or
```
  npm start
```

Server is hosted at port which is defined under `PORT` field in .env file
```
  http://localhost:<PORT>
```
 
    
